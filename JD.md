## 岗位JD
#### [返回](./README.md)
### Go开发
#### 岗位职责：
	参与公司核心产品设计、开发，包括但不限于以下方向：

	-  CloudIaC（基础设施即代码平台）
	-  CloudBoot（裸机云管理平台）
	-  CloudAct2（运维自动化平台）
	-  OPA（通用策略引擎）
	-  Terraform Provider
	-  Provider Registry
	-  Ansible Playbook/Module

	参与产品系统架构设计，核心模块设计和开发，文档编写
	编写工具，提高内部研发效率

#### 任职要求：

1. 全日制大学本科或以上学历，计算机相关专业背景
2. 熟悉 Go 开发语言及框架，有2年以上开发经验
3. 熟悉网络、操作系统知识、常用的数据结构、算法，并能将这些知识应用在实际开发中
4. 熟悉常用数据库（特别是关系型数据库MySQL,PostgreSQL等）及相关知识的实际应用和优化经验
5. 有以下经验者优先：
	- 对Terraform、Ansbile等IaC工具有深入了解
	- 了解 DevOps 及 CI/CD 理念，有相关工具或平台开发经验
	- 开源社区活跃贡献者

* Base：杭州

### 运维开发工程师

* Base：杭州

### Java开发工程师

* Base：武汉

### 高级Java开发工程师

* Base：武汉

#### [返回](./README.md)
